		
	function earth_distance_form_controller(form_selector, start_city_selector, finish_city_selector, calc_ok_callback, calc_error_callback, add_new_form_btn_selector /* optional */) {
		var app = {
			version: '1.0',
			form_selector: form_selector,
			start_city_selector: start_city_selector,
			finish_city_selector: finish_city_selector,
			ok_callback: calc_ok_callback,
			error_callback: calc_error_callback,
			add_new_form_btn_selector: add_new_form_btn_selector
		};
		
		// Пометим форму, что мы её обслуживаем
		$(form_selector).addClass('earth_distance_form_controller_connected');
		
		// .form-distance_between_cities [submit] - обработчик формы
		$(document).on('submit', form_selector, function(){
			if(!earth_distance_form_controller.maps_api) 
				return alert('Загрузка API Яндекс.Карт ещё не завершена...');

			// валидация HTML5 (если поддерживается)
			if(this.reportValidity && this.checkValidity && this.checkValidity() == false) 
				return false;

			var $form = $(this),
				$start_city = $form.find(start_city_selector ),
				$finish_city = $form.find(finish_city_selector);
			if($start_city.val().match(/^ *$/)) 
				return alert($start_city.attr('placeholder')+' не указан!');
			if($finish_city.val().match(/^ *$/))
				return alert($finish_city.attr('placeholder')+' не указан!');
			
			// превяжем переменные $form, $start_city, $finish_city
			(function() {
				var now = new Date();
				
				// заблокируем поля и кнопки
				$form.find('input,button')
					.attr('disabled', 'disabled')
					.addClass('disabled');
				
				// отправим запрос к API Яндекс.Карт
				var route = earth_distance_form_controller.maps_api.route([$start_city.val(), $finish_city.val()]).then(function(route) { 
					var routeLength = route.getLength(),
						start_coord = route.getWayPoints().get(0).geometry.getCoordinates(),
						end_coord = route.getWayPoints().get(1).geometry.getCoordinates();
						
					// Вычесляем растояние в метрах между точками на Земле
					var rad = function(x) { return x * Math.PI / 180; };
					var dLat = rad(end_coord[0] - start_coord[0]);
					var dLong = rad(end_coord[1] - start_coord[1]);
					var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
						Math.cos(rad(start_coord[0])) * Math.cos(rad(end_coord[0])) *
						Math.sin(dLong / 2) * Math.sin(dLong / 2);
					var earth_distance = 6378137 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					
					// передадим результат
					app.ok_callback(earth_distance, routeLength, $form);
					
					// разаблокируем поля и кнопки
					$form.find('input,button')
						.removeAttr('disabled', 'disabled')
						.removeClass('disabled');
				},
				function(error) {
					app.ok_callback(error. $form);
					
					// разаблокируем поля и кнопки
					$form.find('input,button')
						.removeAttr('disabled', 'disabled')
						.removeClass('disabled');
				});
			})();
			
		});
		
		// Кнопка добавления дополнительной строки в форме
		if(!!add_new_form_btn_selector)
		$(document).on('click', add_new_form_btn_selector, function(){
			
			var form_count = $('.earth_distance_form_controller_connected').length; // пригодится ещё не раз
			if(form_count == 10) 
				return alert('Лимит на 10 форм достигнут!');
			
			// скопируем и добавим первую форму как дополнительную
			$new_form = $(form_selector).first()
				.clone()
				.attr('id', 'form'+(form_count+1))
				.insertAfter('.earth_distance_form_controller_connected:last');
		
			// почистим поля и кнопки
			var rus_alphabet = 'АБВГДЕЖЗИКЛМНОПРСТУФ';
			$new_form.find('input,button')
				.removeAttr('disabled')
				.removeClass('disabled');
			$new_form.find(start_city_selector)
				.attr('placeholder', 'Пункт '+rus_alphabet[form_count*2])
				.attr('value', '')
				.val('');
			$new_form.find(finish_city_selector)
				.attr('placeholder', 'Пункт '+rus_alphabet[form_count*2+1])
				.attr('value', '')
				.val('');
		});
		
		// Для старых броузеров используем 2.1.59 (iPhone4, Android 4.4 ...etc)
		$('head').append('<script type="text/javascript" src="http://api-maps.yandex.ru/'+ 
			(!(window.performance||{}).now 
				? '2.1.oldie.1' 
				: '2.1') + 
			'/?load=package.full&amp;lang=ru-RU&amp;onload=earth_distance_form_controller._ymaps_onload"></'+'script>');
	};
	
	// коллбек для загрузки Яндекс.Карт
	earth_distance_form_controller._ymaps_onload = function(ym) {
		earth_distance_form_controller.maps_api = ym;
	}
	
	// функция оформления числа
	earth_distance_form_controller.format_number = function(number) {
		if(number > 999)
			return (Math.round(number)+'').split('').reverse().join('').replace(/^(.)(.)(.)(.)(.)?(.)?(.)?(.)?(.)?$/, '$9$8$7&nbsp;$6$5$4&nbsp;$3$2$1').replace(/^(&nbsp;)+/, '');
		else
			return Math.round(number)+'';
	}